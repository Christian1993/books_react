import React from "react";
import FilterBox from "../FilterBox/FilterBox";
import Books from "../books/Books";

const Home = props => {
  return (
    <main className="home">
      <section className="filter">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-6 offset-md-3">
              <FilterBox />
            </div>
          </div>
        </div>
      </section>
      <section className="books-section">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <Books />
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default Home;
