import React, { Component } from "react";
import { connect } from "react-redux";
import { getBooks, setBooksToShow } from "../../store/actions/booksActions";

class FilterBox extends Component {
  state = {
    filter: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.getBooks(this.state.filter);
    this.props.setBooksToShow(4);
  };

  render() {
    return (
      <div className="filterBox">
        <p className="filterBox__title">Search for a book</p>
        <form className="filterBox__form" onSubmit={this.handleSubmit}>
          <input
            type="text"
            className="input"
            name="filter"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <button className="button">Search</button>
        </form>
      </div>
    );
  }
}

export default connect(
  null,
  { getBooks, setBooksToShow }
)(FilterBox);
