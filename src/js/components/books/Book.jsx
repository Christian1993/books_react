import React, { Component } from "react";
import placeholder from "../../../img/placeholder.png";

export default class Book extends Component {
  handleDescription = description => {
    if (!description) {
      return "No description is available for the moment. We are sorry for inconvenience.";
    }
    const arrayDesc = description
      .split(" ")
      .slice(0, 40)
      .reverse();
    const lastDot = arrayDesc.findIndex(el => el.includes("."));
    if (lastDot >= 0) {
      return arrayDesc
        .slice(lastDot)
        .reverse()
        .join(" ");
    } else {
      return arrayDesc.reverse().join(" ") + "...";
    }
  };
  render() {
    const {
      title = "No title available",
      description,
      imageLinks: { thumbnail = placeholder } = {}
    } = this.props.book.volumeInfo;

    return (
      <div className="col-sm-12 col-md-6">
        <div className="book">
          <img className="book__cover" src={thumbnail} alt="book cover" />
          <div className="book__info">
            <p className="book__title">{title}</p>
            <p className="book__description">
              {this.handleDescription(description)}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
