import React, { Component } from "react";
import { connect } from "react-redux";

import Book from "./Book";
import { setBooksToShow } from "../../store/actions/booksActions";

class Books extends Component {
  scrollEvent = () => {
    window.onscroll = () => {
      const { booksToShow, booksCount } = this.props;
      if (booksCount < booksToShow) return;
      if (
        window.innerHeight + document.documentElement.scrollTop ===
        document.documentElement.offsetHeight
      ) {
        this.props.setBooksToShow(booksToShow + 2);
      }
    };
  };

  componentDidMount() {
    this.scrollEvent();
  }
  componentWillUnmount() {
    console.log("unmount");
  }

  render() {
    const { books, booksToShow } = this.props;
    return (
      <div className="books">
        <div className="row">
          {books &&
            books
              .slice(0, booksToShow)
              .map((book, i) => <Book book={book} key={i} />)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    books: state.books.books,
    booksCount: state.books.booksCount,
    booksToShow: state.books.booksToShow
  };
};

export default connect(
  mapStateToProps,
  { setBooksToShow }
)(Books);
