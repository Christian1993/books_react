import React, { Component } from "react";
import Home from "./components/pages/Home";
import { Provider } from "react-redux";
import store from "./store/store";

class App extends Component {
  render() {
    return (
      <>
        <Provider store={store}>
          <Home />
        </Provider>
      </>
    );
  }
}

export default App;
