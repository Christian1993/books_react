import { GET_BOOKS, SET_BOOKS_TO_SHOW } from "../actions/types";

const initialState = {
  books: [],
  booksCount: 0,
  booksToShow: 4
};

const booksReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BOOKS: {
      return {
        ...state,
        books: action.payload,
        booksCount: action.payload.length
      };
    }
    case SET_BOOKS_TO_SHOW: {
      return {
        ...state,
        booksToShow: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default booksReducer;
