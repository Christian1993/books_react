import { GET_BOOKS, SET_BOOKS_TO_SHOW } from "./types";
import axios from "axios";

export const getBooks = filter => async dispatch => {
  try {
    const response = await axios.get(
      `https://www.googleapis.com/books/v1/volumes?q=${filter}`
    );
    const books = response.data.items;
    dispatch({
      type: GET_BOOKS,
      payload: books
    });
  } catch (err) {
    alert("Ups... Something gone wrong. Please try again.");
  }
};

export const setBooksToShow = num => dispatch => {
  dispatch({
    type: SET_BOOKS_TO_SHOW,
    payload: num
  });
};
