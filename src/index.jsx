import React from "react";
import ReactDOM from "react-dom";
import App from "./js/App";

import "bootstrap/dist/css/bootstrap-grid.css";
import "./sass/main.sass";

ReactDOM.render(<App />, document.getElementById("root"));
