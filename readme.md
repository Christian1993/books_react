# Search for a book

#### Description:
 Simple application allows user to search specific book by its title. Data is fetched by axios. There is also implemented infinite scroll.

#### Technologies used:
* react,
* redux,
* axios,
* sass

#### How to run:
1. Download repo
2. In main directory type: _npm_ _install_
3. Type: _npm_ _run_ _start_
